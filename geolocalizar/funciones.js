var Instasisas = {};

Instasisas.getPixels = function(src){
  var image = new Image();
  image.src = src;
	var c = this.getCanvas(image.width,image.height);
	var ctx = c.getContext("2d");
	ctx.drawImage(image,0,0,c.width,c.height);
	return ctx.getImageData(0,0,c.width,c.height);
}

Instasisas.getCanvas = function(w,h){
	var c = document.createElement("canvas");
  c.id ="mYcanvas";
	c.width = w;
	c.height = h;
	return c;
}




function render(src,opcion){
    var image = new Image();
    image.src = src;
    image.onload = function(){

      if(opcion != "revertir"){
      var c = Instasisas.getCanvas(this.width/2,this.height/2);
      var ctx = c.getContext('2d');
      ctx.drawImage(this,0,0,this.width/2, this.height/2);
    }else{
      var c = Instasisas.getCanvas(this.width,this.height);
      var ctx = c.getContext('2d');
      ctx.drawImage(this,0,0,this.width, this.height);
    }

      var div = document.getElementById("drop");

      removeAllChilds("drop");
      var p = document.createElement("p");
      p.innerHTML = "Original";
      div.appendChild(p);
      div.appendChild(c);
      document.body.appendChild(div);

      var canvas = document.getElementsByTagName("canvas")[0];
      var canvas2 = document.getElementById("mYcanvas");
      url = canvas2.toDataURL();
      //var check = document.getElementById("byn").checked = false;
    }
}


    function addCanvas(imageData,refCanvas,title){

      var resultCanvas = document.createElement("canvas");
      resultCanvas.id = "mYcanvas";
      resultCanvas.width = refCanvas.width;
      resultCanvas.height = refCanvas.height;
      var ctx = resultCanvas.getContext('2d');
      ctx.putImageData(imageData,0,0);
        var div = document.getElementById("drop");
     // removeAllChilds("drop");
      var p = document.createElement("p");
      p.innerHTML = title;
      div.appendChild(p);
      div.appendChild(resultCanvas);
      document.body.appendChild(div);

    }


function removeAllChilds(a)
 {
 var a=document.getElementById(a);
 while(a.hasChildNodes())
  a.removeChild(a.firstChild);  
 }


  